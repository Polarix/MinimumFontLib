/***************************************************************
 * Name:      TextFontToolApp.h
 * Purpose:   Defines Application Class
 * Author:    Polarix (326684221@qq.com)
 * Created:   2018-09-07
 * Copyright: Polarix (https://gitee.com/Polarix)
 * License:
 **************************************************************/

#ifndef _INLCUDE_CLASS_TEXT_FONT_TOOL_
#define _INLCUDE_CLASS_TEXT_FONT_TOOL_

#include <wx/app.h>
#include <wx/intl.h>

#include "MainFrame.h"

class MinimumFontLibApp : public wxApp
{
	private:
		MainFrame*				m_pclsMainFrame;
		wxLocale					m_clsLocale;
    public:
        virtual bool				OnInit();
        virtual void				SetLanguage(wxLanguage eLanguage);
};

#endif // _INLCUDE_CLASS_TEXT_FONT_TOOL_
