/***************************************************************
 * Name:      TextFontToolApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Polarix (326684221@qq.com)
 * Created:   2018-09-07
 * Copyright: Polarix (https://gitee.com/Polarix)
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__


#include "Application.h"

IMPLEMENT_APP(MinimumFontLibApp);

bool MinimumFontLibApp::OnInit()
{
	/* Add image handler. */
    wxImage::AddHandler(new wxPNGHandler);
	//wxImage::AddHandler(new wxJPEGHandler);
	/* Set language. */
	SetLanguage(wxLANGUAGE_DEFAULT);

	m_pclsMainFrame = new MainFrame((wxWindow*)NULL);
    m_pclsMainFrame->SetIcon(wxICON(aaaa)); // To Set App Icon
    m_pclsMainFrame->Show();

    return true;
}

void MinimumFontLibApp::SetLanguage(wxLanguage eLanguage)
{
	bool			bLocaleResult;
    wxLocale::AddCatalogLookupPathPrefix("../Lang");
    bLocaleResult = m_clsLocale.Init(eLanguage);
	if(false == bLocaleResult)
    {
        wxLogWarning(_("This language is not supported by the system."));
	}
    bLocaleResult = m_clsLocale.AddCatalog("Lang");
    if (false == bLocaleResult)
    {
        printf("Load language file failed.");
        // No language files.
    }
}
