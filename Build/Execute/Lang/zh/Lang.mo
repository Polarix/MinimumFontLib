��          �      |      �     �     �  =        L     \     u  
   �     �     �     �  	   �     �     �     �  	   �  
   �     �     �     �  �  	     �    �     �     �  '        -  $   =     b     o     {     �     �     �  	   �  	   �     �     �     �     �  	   �     �  }    C   �	                            
         	                                                            About About this application Content is already changed, Do you want to save your changes? Create new file Extract and recode text. Extracted Text Full ASCII New Open Open a text file. Open file Recode Recode text Save Save file Save file. Save to file Source Text Text Font Tool This application is used for create a minimized font library for monochrome LCD.
On the embedded platform, flash and ram resource are usually not enough to include a complete font lib(like GB-3212). But you can use this application to extract and recoding all the character you use. It can help you control the memory consumption by the font lib.
Unfortunately, It must also be used with font modulo software, but I will complete this function.
 Use full ASCII coed. Project-Id-Version: Text Font Tool
POT-Creation-Date: 2018-09-14 15:54+0800
PO-Revision-Date: 2018-09-14 15:55+0800
Last-Translator: 
Language-Team: Polarix
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.2
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _I18N
X-Poedit-SearchPath-0: Application
X-Poedit-SearchPath-1: Common
X-Poedit-SearchPath-2: Frames
X-Poedit-SearchPath-3: Dialogs
 关于 关于 内容已被修改，是否要保存？ 创建新文件 提取使用文字并重新编码。 提取文字 标准ASCII 新建 打开 打开已有文件。 打开文件 重编码 重编码 保存 保存文件 保存文件。 保存到文件 源文本 Text Font Tool 此程序用于为单色LCD创建最小化的字体库。
在嵌入式平台上，Flash资源通常不足以包含完整的字体库（如GB-3212）。此程序可以提取和重新编码您使用的所有字符。 它可以帮助您精简因字库导致的Flash消耗。
目前它必须与字体取模软件一起使用，但我会尽快为此程序编写并完善取模功能。
 使用完整ASCII编码库，不对ASCII字符进行重新编码。 