#ifndef _INCLUDED_ABOUT_DIALOG_H_
#define _INCLUDED_ABOUT_DIALOG_H_

#include <wx/dialog.h>
#include <wx/textctrl.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include "Common.h"

class AboutDialog : public wxDialog
{
	private:
		wxTextCtrl* m_pclsTextCtrl_InfoText;

	protected:

	public:

		AboutDialog(wxWindow* pclsParent, wxWindowID iId = wxID_ANY);
		~AboutDialog();

};

#endif // _INCLUDED_ABOUT_DIALOG_H_
