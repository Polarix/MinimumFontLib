#include "AboutDialog.h"

const wxString strAboutText = _I18N(
"This application is used for create a minimized font library for monochrome LCD.\n\
On the embedded platform, flash and ram resource are usually not enough to include \
a complete font lib(like GB-3212). But you can use this application to extract and \
recoding all the character you use. It can help you control the memory consumption \
by the font lib.\n\
Unfortunately, It must also be used with font modulo software, but I will complete \
this function.\n");

AboutDialog::AboutDialog(wxWindow* pclsParent, wxWindowID iId) :
wxDialog(pclsParent, iId, _I18N("About"), wxDefaultPosition, wxSize(320, 320), wxDEFAULT_DIALOG_STYLE)
{
	SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* pclsBoxSizer_RootSizer;
	pclsBoxSizer_RootSizer = new wxBoxSizer( wxVERTICAL );

	m_pclsTextCtrl_InfoText = new wxTextCtrl( this, wxID_ANY, strAboutText, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY|wxNO_BORDER );
	m_pclsTextCtrl_InfoText->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_BTNFACE ) );

	pclsBoxSizer_RootSizer->Add( m_pclsTextCtrl_InfoText, 1, wxALL|wxEXPAND, 5 );


	SetSizer( pclsBoxSizer_RootSizer );
	Layout();
	Centre( wxBOTH );
}

AboutDialog::~AboutDialog()
{
}
