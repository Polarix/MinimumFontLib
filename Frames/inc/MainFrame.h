///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef _INCLUDE_CLASS_TEXT_INPUT_FRAME_H_
#define _INCLUDE_CLASS_TEXT_INPUT_FRAME_H_

#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/toolbar.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/notebook.h>
#include <wx/filedlg.h>

#include "Common.h"

///////////////////////////////////////////////////////////////////////////////
/// Class TextInputFrame
///////////////////////////////////////////////////////////////////////////////
class MainFrame : public wxFrame
{
	DECLARE_EVENT_TABLE();

	private:
		wxToolBar*					m_pclsToolBar_ToolBar;
		wxNotebook*					m_pclsNotebook_NoteBook;
		wxTextCtrl*					m_pclsInputText;
		wxTextCtrl*					m_pclsExtractedText;
		wxTextCtrl*					m_pclsEncodeText;
		wxStatusBar*				m_pclsStatusBar_StatusBar;

		bool						m_bTextChanged;
		wxString					m_strWorkingFileName;

		void						_initialzie(void);
		void						OnNewCreate(wxCommandEvent& clsEvent);
		void						OnOpenFile(wxCommandEvent& clsEvent)			{OpenFile(clsEvent);clsEvent.Skip();}
		void						OnSaveFile(wxCommandEvent& clsEvent)			{SaveFile(clsEvent);clsEvent.Skip();}
		void						OnEncode(wxCommandEvent& clsEvent);
		void						_wxEvent_OnTextChanged(wxCommandEvent& clsEvent)	{OnTextChanged(clsEvent);clsEvent.Skip();}
		void						_wxEvent_OnAbout(wxCommandEvent& clsEvent)			{About(clsEvent);clsEvent.Skip();}

		int							_openFile(void);
		int							_saveFile(void);

	protected:
		virtual void				OpenFile(wxCommandEvent& clsEvent);
		virtual void				SaveFile(wxCommandEvent& clsEvent);
		virtual void				OnTextChanged(wxCommandEvent& clsEvent);
		virtual void				About(wxCommandEvent& clsEvent);

	public:

		MainFrame( wxWindow* pclsParent, wxWindowID iId = wxID_ANY);

		~MainFrame();

};

#endif //_INCLUDE_CLASS_TEXT_INPUT_FRAME_H_
