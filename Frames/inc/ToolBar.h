#ifndef _INCLUDE_TOOLBAR_H_
#define _INCLUDE_TOOLBAR_H_

#include <wx/toolbar.h>
#include <wx/frame.h>

#define TEXT_TOOL_TOOL_ID_NONE		(0)
#define TEXT_TOOL_ID_MAX			(-1)

enum
{
	TEXT_TOOL_TOOL_ID_NEW_FILE = TEXT_TOOL_TOOL_ID_NONE+1,
	TEXT_TOOL_TOOL_ID_OPEN_FILE,
	TEXT_TOOL_TOOL_ID_SAVE_FILE,
	TEXT_TOOL_TOOL_ID_FULL_ASCII,
	TEXT_TOOL_TOOL_ID_RECODE,
	TEXT_TOOL_TOOL_ID_ABOUT,
	TEXT_TOOL_TOOL_ID_EXIT,
};

typedef struct
{
    const int					iEventID;
    wxItemKind                  eToolType;
    const wxString              cszLabel;
    const wxString              cstrEnabledImageResID;
    const wxString              cstrDisabledImageResID;
    const wxString              cstrToolTip;
    const wxString              cstrStatusBarText;
    wxToolBarToolBase*			pclsObject;
}TOOL_BAR_TOOL_DATA;

#ifdef __cplusplus
extern "C" {
#endif

wxToolBar*			CreateToolBar(wxFrame* pclsOwner, TOOL_BAR_TOOL_DATA* pstToolTable);
wxToolBarToolBase*	SearchForTool(int iID, TOOL_BAR_TOOL_DATA* pstToolTable);
wxToolBar*			CreateTextInputToolBar(wxFrame* pclsOwner);
wxToolBarToolBase*	SearchTool(int iID);

#ifdef __cplusplus
}
#endif

#endif // _INCLUDE_TOOLBAR_H_
