///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////
#include <wx/msgdlg.h>
#include <memory.h>

#include "MainFrame.h"
#include "ToolBar.h"
#include "AboutDialog.h"
#include <algorithm>

BEGIN_EVENT_TABLE(MainFrame, wxFrame )
	EVT_TOOL(TEXT_TOOL_TOOL_ID_NEW_FILE, MainFrame::OnNewCreate)
	EVT_TOOL(TEXT_TOOL_TOOL_ID_OPEN_FILE, MainFrame::OnOpenFile)
	EVT_TOOL(TEXT_TOOL_TOOL_ID_SAVE_FILE, MainFrame::OnSaveFile)
	EVT_TOOL(TEXT_TOOL_TOOL_ID_RECODE, MainFrame::OnEncode)
	EVT_TOOL(TEXT_TOOL_TOOL_ID_ABOUT, MainFrame::_wxEvent_OnAbout)
	EVT_TEXT(wxID_ANY, MainFrame::_wxEvent_OnTextChanged)
END_EVENT_TABLE()
///////////////////////////////////////////////////////////////////////////
MainFrame::MainFrame(wxWindow* pclsParent, wxWindowID iId) :
wxFrame(pclsParent, iId, _I18N(APP_NAME), wxDefaultPosition, wxSize(640, 480), wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL)
{
	SetSizeHints( wxDefaultSize, wxDefaultSize );

	m_pclsToolBar_ToolBar = CreateTextInputToolBar(this);

	wxBoxSizer* pclsBoxSizer_RootSizer = new wxBoxSizer( wxVERTICAL );

	m_pclsNotebook_NoteBook = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	m_pclsInputText = new wxTextCtrl(m_pclsNotebook_NoteBook, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE );
	m_pclsExtractedText = new wxTextCtrl(m_pclsNotebook_NoteBook, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE );
	m_pclsEncodeText = new wxTextCtrl(m_pclsNotebook_NoteBook, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE );

	m_pclsNotebook_NoteBook->AddPage(m_pclsInputText, _I18N("Source Text"), true);
	m_pclsNotebook_NoteBook->AddPage(m_pclsExtractedText, _I18N("Extracted Text"));
	m_pclsNotebook_NoteBook->AddPage(m_pclsEncodeText, _I18N("Recode text"));

	pclsBoxSizer_RootSizer->Add(m_pclsNotebook_NoteBook, 1, wxEXPAND, 5 );

	m_pclsStatusBar_StatusBar = this->CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );

	SetSizer( pclsBoxSizer_RootSizer );
	Layout();
	Centre( wxBOTH );

	_initialzie();
}

MainFrame::~MainFrame()
{
}

void MainFrame::_initialzie(void)
{
	m_pclsInputText->Clear();

	m_bTextChanged = false;
	m_strWorkingFileName = wxEmptyString;
	m_pclsStatusBar_StatusBar->SetLabel(m_strWorkingFileName);
}

void MainFrame::OnNewCreate(wxCommandEvent& clsEvent)
{
	if(true == m_bTextChanged)
	{
		int			iMsgResult;

		iMsgResult = wxMessageBox(_I18N("Content is already changed, Do you want to save your changes?"), _I18N("Save file."), wxYES_NO|wxCANCEL|wxCENTER, this);

		switch(iMsgResult)
		{
			case wxCANCEL:
			{
				break;
			}
			case wxYES:
			{
				if(wxID_CANCEL == _saveFile())
				{
					break;
				}
				// no break;
			}
			case wxNO:
			{
				_initialzie();
				break;
			}
			default:
			{
				break;
			}
		}
	}
	else
	{
		_initialzie();
	}
}

void MainFrame::OpenFile(wxCommandEvent& clsEvent)
{
	if(true == m_bTextChanged)
	{
		int			iMsgResult;

		iMsgResult = wxMessageBox(_I18N("Content is already changed, Do you want to save your changes?"), _I18N("Save file."), wxYES_NO|wxCANCEL|wxCENTER, this);

		switch(iMsgResult)
		{
			case wxCANCEL:
			{
				break;
			}
			case wxYES:
			{
				if(wxID_CANCEL == _saveFile())
				{
					break;
				}
				// no break;
			}
			case wxNO:
			{
				_openFile();
				m_bTextChanged = false;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	else
	{
		_openFile();
		m_bTextChanged = false;
	}
}

void MainFrame::SaveFile(wxCommandEvent& clsEvent)
{
    _saveFile();
    m_bTextChanged = false;
}

void MainFrame::OnTextChanged(wxCommandEvent& clsEvent)
{
	m_bTextChanged = true;
}

void MainFrame::OnEncode(wxCommandEvent& clsEvent)
{
	wxString			strContent = m_pclsInputText->GetValue();

    //Deduplication
	if(strContent.Len() > 1)
    {
        for(wxString::iterator clsIterRef = strContent.begin(); clsIterRef != strContent.end(); ++clsIterRef)
        {
            // Search from the next of clsIterRef;
            wxString::iterator clsIterCmp = clsIterRef+1;
            while(clsIterCmp != strContent.end())
            {
                if(*clsIterCmp == *clsIterRef)
                {
                    clsIterCmp = strContent.erase(clsIterCmp);
                }
                else
                {
                    ++clsIterCmp;
                }
            }
        }
        // Sort
        for(wxString::iterator clsIterLeft = strContent.begin(); clsIterLeft != strContent.end(); ++clsIterLeft)
        {
            // Search from the next of clsIterRef;
            wxString::iterator clsIterRight = clsIterLeft+1;
            while(clsIterRight != strContent.end())
            {
                if(*clsIterLeft > *clsIterRight)
                {
                    wxChar cSwapChar = *clsIterLeft;
                    *clsIterLeft = *clsIterRight;
                    *clsIterRight = cSwapChar;
                }
                ++clsIterRight;
            }
        }
    }
    m_pclsExtractedText->SetValue(strContent);

    // Encode
    m_pclsEncodeText->Clear();
    for(wxString::iterator clsIterLeft = strContent.begin(); clsIterLeft != strContent.end(); ++clsIterLeft)
    {
        m_pclsEncodeText->AppendText(wxString::Format(wxT("0x%04X, // U+%04X  %c\n"), *clsIterLeft, *clsIterLeft, *clsIterLeft));
    }
	m_pclsNotebook_NoteBook->SetSelection(1);

	clsEvent.Skip();
}

void MainFrame::About(wxCommandEvent& clsEvent)
{
    AboutDialog* pclsAboutDialog = new AboutDialog(this);
    if(NULL != pclsAboutDialog)
	{
		pclsAboutDialog->ShowModal();
	}
}

int MainFrame::_openFile(void)
{
	int			iMsgResult = wxID_CANCEL;

	if(NULL != m_pclsInputText)
	{
		wxFileDialog clsFileDlg(this, _I18N("Open file"), wxEmptyString, wxEmptyString, wxT("Text file(*.txt)|*.txt|All files|(*.*)"));

		iMsgResult = clsFileDlg.ShowModal();

		if(wxID_OK == iMsgResult)
		{
			m_strWorkingFileName = clsFileDlg.GetPath();
			m_pclsInputText->LoadFile(m_strWorkingFileName);
		}

		m_pclsStatusBar_StatusBar->SetLabel(m_strWorkingFileName);
	}

	return iMsgResult;
}

int MainFrame::_saveFile(void)
{
	int			iMsgResult = wxID_CANCEL;

	if(NULL != m_pclsInputText)
	{
		if(wxEmptyString == m_strWorkingFileName)
		{
			wxFileDialog clsFileDlg(this, _I18N("Save file"), wxEmptyString, wxEmptyString, wxT("Text file(*.txt)|*.txt|All files|(*.*)"));

			iMsgResult = clsFileDlg.ShowModal();

			if(wxID_OK == iMsgResult)
			{
				m_strWorkingFileName = clsFileDlg.GetPath();
			}
		}
		else
		{
			iMsgResult = wxID_OK;
		}
		m_pclsInputText->SaveFile(m_strWorkingFileName);

		m_pclsStatusBar_StatusBar->SetLabel(m_strWorkingFileName);
	}

	return iMsgResult;
}

