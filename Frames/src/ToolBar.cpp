#include "ToolBar.h"
#include "Common.h"

TOOL_BAR_TOOL_DATA MAIN_FRAME_TOOLBAR_TABLE[] =
{
    {TEXT_TOOL_TOOL_ID_NEW_FILE,		wxITEM_NORMAL,		wxEmptyString,	wxT("RES_ID_TOOL_IMAGE_NEW_FILE"),		wxT("RES_ID_TOOL_IMAGE_NEW_FILE_DISABLE"),		_I18N("New"),			_I18N("Create new file"), NULL},
    {TEXT_TOOL_TOOL_ID_OPEN_FILE,		wxITEM_NORMAL,		wxEmptyString,	wxT("RES_ID_TOOL_IMAGE_OPEN_FILE"),		wxT("RES_ID_TOOL_IMAGE_OPEN_FILE_DISABLE"),		_I18N("Open"),			_I18N("Open a text file."), NULL},
    {TEXT_TOOL_TOOL_ID_SAVE_FILE,		wxITEM_NORMAL,		wxEmptyString,	wxT("RES_ID_TOOL_IMAGE_SAVE_FILE"),		wxT("RES_ID_TOOL_IMAGE_SAVE_FILE_DISABLE"),		_I18N("Save"),			_I18N("Save to file"), NULL},
    {TEXT_TOOL_TOOL_ID_NONE,			wxITEM_SEPARATOR,	wxEmptyString,	wxEmptyString, wxEmptyString, wxEmptyString, wxEmptyString, NULL},
    {TEXT_TOOL_TOOL_ID_FULL_ASCII,		wxITEM_CHECK,		wxEmptyString,	wxT("RES_ID_TOOL_IMAGE_ASCII"),			wxT("RES_ID_TOOL_IMAGE_ASCII_DISABLE"),			_I18N("Full ASCII"),	_I18N("Use full ASCII coed."), NULL},
    {TEXT_TOOL_TOOL_ID_RECODE,			wxITEM_NORMAL,		wxEmptyString,	wxT("RES_ID_TOOL_IMAGE_RECODE"),		wxT("RES_ID_TOOL_IMAGE_RECODE_DISABLE"),		_I18N("Recode"),		_I18N("Extract and recode text."), NULL},
    {TEXT_TOOL_TOOL_ID_NONE,			wxITEM_SEPARATOR,	wxEmptyString,	wxEmptyString, wxEmptyString, wxEmptyString, wxEmptyString, NULL},
    {TEXT_TOOL_TOOL_ID_ABOUT,			wxITEM_NORMAL,		wxEmptyString,	wxT("RES_ID_TOOL_IMAGE_ABOUT"),			wxT("RES_ID_TOOL_IMAGE_ABOUT_DISABLE"),			_I18N("About"),			_I18N("About this application"), NULL},
    {TEXT_TOOL_ID_MAX, wxITEM_NORMAL,	wxEmptyString,		wxEmptyString,	wxEmptyString, wxEmptyString, wxEmptyString, NULL},
};

wxToolBar* CreateToolBar(wxFrame* pclsOwner, TOOL_BAR_TOOL_DATA* pstToolTable)
{
    TOOL_BAR_TOOL_DATA*			pstToolData;
    wxToolBar*					pclsToolBar;

    pclsToolBar = pclsOwner->CreateToolBar(wxTB_HORIZONTAL);
    if(NULL != pclsToolBar)
	{
		pstToolData = pstToolTable;
		if(NULL != pstToolData)
		{
			while(TEXT_TOOL_ID_MAX != pstToolData->iEventID)
			{
				if(wxITEM_SEPARATOR == pstToolData->eToolType)
				{
					pstToolData->pclsObject = pclsToolBar->AddSeparator();
				}
				else
				{
					pstToolData->pclsObject = pclsToolBar->AddTool( pstToolData->iEventID,
																	  _(pstToolData->cszLabel),
																	  wxBitmap( pstToolData->cstrEnabledImageResID, wxBITMAP_TYPE_PNG_RESOURCE ),
																	  wxNullBitmap,
																	  pstToolData->eToolType,
																	  _(pstToolData->cstrToolTip),
																	  _(pstToolData->cstrStatusBarText),
																	  NULL );
				}
				pstToolData++;
			}
		}
		pclsToolBar->Realize();
	}

    return pclsToolBar;
}

wxToolBarToolBase* SearchForTool(int iID, TOOL_BAR_TOOL_DATA* pstToolTable)
{
	wxToolBarToolBase*			pclsToolObject;
	TOOL_BAR_TOOL_DATA*			pstToolData;

	pstToolData = pstToolTable;
	pclsToolObject = NULL;

	while(TEXT_TOOL_ID_MAX != pstToolData->iEventID)
	{
		if(iID == pstToolData->iEventID)
		{
			pclsToolObject = pstToolData->pclsObject;
			break;
		}
		else
		{
			pstToolData++;
		}
	}

	return pclsToolObject;
}

wxToolBar* CreateTextInputToolBar(wxFrame* pclsOwner)
{
	return CreateToolBar(pclsOwner, MAIN_FRAME_TOOLBAR_TABLE);
}

wxToolBarToolBase* SearchTool(int iID)
{
	return SearchForTool(iID, MAIN_FRAME_TOOLBAR_TABLE);
}
